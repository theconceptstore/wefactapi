# README #

Initial version of the WeFact Hosting API, Work in progress.

# Example for retrieving a product group list#

```
#!php
<?php

$products = Tcsehv\WeFact\WeFactApi::product()->addParam('group', 4)->getlist()->execute();
```
Sometimes, it isn't required to filter data. Then you can ignore the addParam()-function.