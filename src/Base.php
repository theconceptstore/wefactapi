<?php
/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
 */

namespace Tcsehv\WeFact;

use GuzzleHttp\Client;
use GuzzleHttp\Event\CompleteEvent;
use GuzzleHttp\Message\Response;

/**
 * Class Base
 * @package Tcsehv\WeFact
 */
class Base {

    /**
     * @var null
     */
    private $apiUrl = null;

    /**
     * @var null
     */
    private $apiKey = null;

    /**
     * @var null
     */
    protected $apiController = null;

    /**
     * @var null
     */
    protected $apiAction = null;

    /**
     * @var array
     */
    protected $methodParameters = array();

    /**
     * @param $apiUrl
     * @param $apiKey
     * @return $this
     */
    public function setApiConfig($apiUrl,$apiKey) {
        $this->apiUrl = $apiUrl;
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function addParam($key, $value) {
        $this->methodParameters[$key] = $value;

        return $this;
    }

    /**
     * @param array $allowedParams
     * @return array
     */
    protected function checkParams(array $allowedParams)
    {
        if (!empty($this->methodParameters)) {
            foreach ($this->methodParameters as $key => $value) {
                if (!in_array($key, $allowedParams)) {
                    unset($this->methodParameters[$key]);
                }
            }
        }
        return $this->methodParameters;
    }

    /**
     * @return string|object
     * @throws \Exception
     */
    public function execute() {
        // Check if API Key and API Url are set
        if(!$this->apiKey || !$this->apiUrl) {
            throw new \Exception('Api settings are not set');
        }

        if(is_array($this->methodParameters)){
            $this->methodParameters['api_key'] 		= $this->apiKey;
            $this->methodParameters['controller'] 	= ucfirst($this->apiController);
            $this->methodParameters['action'] 		= ucfirst($this->apiAction);
        }

        // Setup a new client for requesting the API data
        $client = new Client();
        $request = $client->createRequest('POST', $this->apiUrl);
        foreach($this->methodParameters as $key => $value) {
            $request->getBody()->setField($key, $value);
        }
        $cachedResponse = new Response(200);

        $request->getEmitter()->on(
            'complete',
            function (CompleteEvent $e) use ($cachedResponse) {
                if ($e->getResponse()->getStatusCode() == 500) {
                    throw new \Exception('There was a problem while processing the request on the API: '.$e->getResponse()->getReasonPhrase());
                }
            }
        );

        // Send the request and return the data
        $response = $client->send($request);
        return json_decode($response->getBody());
    }
}