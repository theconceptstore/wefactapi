<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
*/
class Handle extends Base implements ConnectionInterface {

	protected $apiController = "Handle";

	/**
	 * @param string $DebtorCode
	 * @param string $copyDataFromDebtor
	 * @return \Tcsehv\WeFact\Methods\Handle
	*/
	public function add($DebtorCode,$copyDataFromDebtor) {
		$this->apiAction = "add";
		$this->methodParameters = array(
			"DebtorCode" => $DebtorCode,
			"copyDataFromDebtor" => $copyDataFromDebtor,
		);
		return $this;
	}

	/**
	 * @param string $Handle
	 * @return \Tcsehv\WeFact\Methods\Handle
	*/
	public function delete($Handle) {
		$this->apiAction = "delete";
		$this->methodParameters = array(
			"Handle" => $Handle,
		);
		return $this;
	}

	/**
	 * @param string $Handle
	 * @param string $Registrar
	 * @param string $PhoneNumber
	 * @return \Tcsehv\WeFact\Methods\Handle
	*/
	public function edit($Handle,$Registrar,$PhoneNumber) {
		$this->apiAction = "edit";
		$this->methodParameters = array(
			"Handle" => $Handle,
			"Registrar" => $Registrar,
			"PhoneNumber" => $PhoneNumber,
		);
		return $this;
	}

	/**
	 * @param string $searchfor
	 * @return \Tcsehv\WeFact\Methods\Handle
	*/
	public function getlist($searchfor) {
		$this->apiAction = "list";
		$this->methodParameters = array(
			"searchfor" => $searchfor,
		);
		return $this;
	}

	/**
	 * @param string $Handle
	 * @return \Tcsehv\WeFact\Methods\Handle
	*/
	public function listdomain($Handle) {
		$this->apiAction = "listdomain";
		$this->methodParameters = array(
			"Handle" => $Handle,
		);
		return $this;
	}

	/**
	 * @param string $Handle
	 * @return \Tcsehv\WeFact\Methods\Handle
	*/
	public function show($Handle) {
		$this->apiAction = "show";
		$this->methodParameters = array(
			"Handle" => $Handle,
		);
		return $this;
	}

}