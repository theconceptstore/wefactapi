<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
*/
class Debtor extends Base implements ConnectionInterface {

	protected $apiController = "Debtor";

    /**
     * Add new debtor.
     *
     * For more params: https://www.wefact.nl/wefact-hosting/apiv2/debiteuren/add
     *
     * @param array $data
     * @return Debtor
     */
	public function add(array $data) {
		$this->apiAction = "add";
		$this->methodParameters = $data;

        // Validate parameters
        $allowedParams = ['Address', 'InvoiceAddress', 'InvoiceCompanyName', 'InvoiceSex', 'InvoiceCountry', 'InvoiceCity',
            'InvoiceEmailAddress', 'InvoiceHouseNumber', 'InvoiceName', 'InvoiceZipCode', 'CompanyName', 'Country',
            'City', 'EmailAddress', 'FaxNumber', 'HouseNumber', 'MobileNumber', 'Initials', 'SurName', 'ZipCode', 'Sex', 'PhoneNumber',
            'Website', 'AccountNumber', 'AccountName', 'AccountBank', 'AccountCity', 'AccountBIC', 'InvoiceAuthorisation',
            'DefaultLanguage', 'ActiveLogin', 'Website'];
        $this->checkParams($allowedParams);

		return $this;
	}

	/**
	 * @param string $Username
	 * @param string $Password
	 * @return \Tcsehv\WeFact\Methods\Debtor
	*/
	public function checklogin($Username,$Password) {
		$this->apiAction = "checklogin";
		$this->methodParameters = array(
			"Username" => $Username,
			"Password" => $Password,
		);
		return $this;
	}

	/**
	 * @param string $Identifier
	 * @param string $CompanyName
	 * @param string $AccountName
	 * @return \Tcsehv\WeFact\Methods\Debtor
	*/
	public function edit($Identifier,$CompanyName,$AccountName) {
		$this->apiAction = "edit";
		$this->methodParameters = array(
			"Identifier" => $Identifier,
			"CompanyName" => $CompanyName,
			"AccountName" => $AccountName,
		);
		return $this;
	}

	/**
	 * @param string $searchat
	 * @param string $searchfor
	 * @return \Tcsehv\WeFact\Methods\Debtor
	*/
	public function getlist($searchat,$searchfor) {
		$this->apiAction = "list";
		$this->methodParameters = array(
			"searchat" => $searchat,
			"searchfor" => $searchfor,
		);
		return $this;
	}

	/**
	 * @param string $Identifier
	 * @return \Tcsehv\WeFact\Methods\Debtor
	*/
	public function show($Identifier) {
		$this->apiAction = "show";
		$this->methodParameters = array(
			"Identifier" => $Identifier,
		);
		return $this;
	}

}