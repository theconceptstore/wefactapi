<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
*/
class Hosting extends Base implements ConnectionInterface {

	protected $apiController = "Hosting";

	/**
	 * @param string $DebtorCode
	 * @param string $Username
	 * @param string $Domain
	 * @param string $Package
	 * @param string $HasSubscription
	 * @param array $Subscription
	 * @return \Tcsehv\WeFact\Methods\Hosting
	*/
	public function add($DebtorCode,$Username,$Domain,$Package,$HasSubscription,$Subscription) {
		$this->apiAction = "add";
		$this->methodParameters = array(
			"DebtorCode" => $DebtorCode,
			"Username" => $Username,
			"Domain" => $Domain,
			"Package" => $Package,
			"HasSubscription" => $HasSubscription,
			"Subscription" => $Subscription,
		);
		return $this;
	}

	/**
	 * @param string $Username
	 * @return \Tcsehv\WeFact\Methods\Hosting
	*/
	public function create($Username) {
		$this->apiAction = "create";
		$this->methodParameters = array(
			"Username" => $Username,
		);
		return $this;
	}

	/**
	 * @param string $Username
	 * @return \Tcsehv\WeFact\Methods\Hosting
	*/
	public function delete($Username) {
		$this->apiAction = "delete";
		$this->methodParameters = array(
			"Username" => $Username,
		);
		return $this;
	}

	/**
	 * @param string $Username
	 * @param string $Status
	 * @return \Tcsehv\WeFact\Methods\Hosting
	*/
	public function edit($Username,$Status) {
		$this->apiAction = "edit";
		$this->methodParameters = array(
			"Username" => $Username,
			"Status" => $Status,
		);
		return $this;
	}

	/**
	 * @param string $Username
	 * @return \Tcsehv\WeFact\Methods\Hosting
	*/
	public function getdomainlist($Username) {
		$this->apiAction = "getdomainlist";
		$this->methodParameters = array(
			"Username" => $Username,
		);
		return $this;
	}

	/**
	 * @param string $searchat
	 * @param string $searchfor
	 * @return \Tcsehv\WeFact\Methods\Hosting
	*/
	public function getlist($searchat,$searchfor) {
		$this->apiAction = "list";
		$this->methodParameters = array(
			"searchat" => $searchat,
			"searchfor" => $searchfor,
		);
		return $this;
	}

	/**
	 * @param string $Identifier
	 * @return \Tcsehv\WeFact\Methods\Hosting
	*/
	public function removefromserver($Identifier) {
		$this->apiAction = "removefromserver";
		$this->methodParameters = array(
			"Identifier" => $Identifier,
		);
		return $this;
	}

	/**
	 * @param string $Username
	 * @return \Tcsehv\WeFact\Methods\Hosting
	*/
	public function sendaccountinfobyemail($Username) {
		$this->apiAction = "sendaccountinfobyemail";
		$this->methodParameters = array(
			"Username" => $Username,
		);
		return $this;
	}

	/**
	 * @param string $Username
	 * @return \Tcsehv\WeFact\Methods\Hosting
	*/
	public function show($Username) {
		$this->apiAction = "show";
		$this->methodParameters = array(
			"Username" => $Username,
		);
		return $this;
	}

	/**
	 * @param string $Username
	 * @return \Tcsehv\WeFact\Methods\Hosting
	*/
	public function suspend($Username) {
		$this->apiAction = "suspend";
		$this->methodParameters = array(
			"Username" => $Username,
		);
		return $this;
	}

	/**
	 * @param string $Identifier
	 * @return \Tcsehv\WeFact\Methods\Hosting
	*/
	public function terminate($Identifier) {
		$this->apiAction = "terminate";
		$this->methodParameters = array(
			"Identifier" => $Identifier,
		);
		return $this;
	}

	/**
	 * @param string $Username
	 * @return \Tcsehv\WeFact\Methods\Hosting
	*/
	public function unsuspend($Username) {
		$this->apiAction = "unsuspend";
		$this->methodParameters = array(
			"Username" => $Username,
		);
		return $this;
	}

	/**
	 * @param string $Username
	 * @param string $ProductCode
	 * @param string $Periods
	 * @param string $Periodic
	 * @param string $InvoiceCycle
	 * @param string $CreateInvoice
	 * @return \Tcsehv\WeFact\Methods\Hosting
	*/
	public function updowngrade($Username,$ProductCode,$Periods,$Periodic,$InvoiceCycle,$CreateInvoice) {
		$this->apiAction = "updowngrade";
		$this->methodParameters = array(
			"Username" => $Username,
			"ProductCode" => $ProductCode,
			"Periods" => $Periods,
			"Periodic" => $Periodic,
			"InvoiceCycle" => $InvoiceCycle,
			"CreateInvoice" => $CreateInvoice,
		);
		return $this;
	}

}