<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
*/
class Orderline extends Base implements ConnectionInterface {

	protected $apiController = "Orderline";

	/**
	 * @param string $OrderCode
	 * @param array $OrderLines
	 * @return \Tcsehv\WeFact\Methods\Orderline
	*/
	public function add($OrderCode,$OrderLines) {
		$this->apiAction = "add";
		$this->methodParameters = array(
			"OrderCode" => $OrderCode,
			"OrderLines" => $OrderLines,
		);
		return $this;
	}

	/**
	 * @param string $OrderCode
	 * @param array $OrderLines
	 * @return \Tcsehv\WeFact\Methods\Orderline
	*/
	public function delete($OrderCode,$OrderLines) {
		$this->apiAction = "delete";
		$this->methodParameters = array(
			"OrderCode" => $OrderCode,
			"OrderLines" => $OrderLines,
		);
		return $this;
	}

}