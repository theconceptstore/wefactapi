<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
*/
class Pricequote extends Base implements ConnectionInterface {

	protected $apiController = "Pricequote";

	/**
	 * @param string $PriceQuoteCode
	 * @return \Tcsehv\WeFact\Methods\Pricequote
	*/
	public function accept($PriceQuoteCode) {
		$this->apiAction = "accept";
		$this->methodParameters = array(
			"PriceQuoteCode" => $PriceQuoteCode,
		);
		return $this;
	}

	/**
	 * @param string $DebtorCode
	 * @param array $PriceQuoteLines
	 * @return \Tcsehv\WeFact\Methods\Pricequote
	*/
	public function add($DebtorCode,$PriceQuoteLines) {
		$this->apiAction = "add";
		$this->methodParameters = array(
			"DebtorCode" => $DebtorCode,
			"PriceQuoteLines" => $PriceQuoteLines,
		);
		return $this;
	}

	/**
	 * @param string $PriceQuoteCode
	 * @return \Tcsehv\WeFact\Methods\Pricequote
	*/
	public function decline($PriceQuoteCode) {
		$this->apiAction = "decline";
		$this->methodParameters = array(
			"PriceQuoteCode" => $PriceQuoteCode,
		);
		return $this;
	}

	/**
	 * @param string $PriceQuoteCode
	 * @param string $DeleteType
	 * @return \Tcsehv\WeFact\Methods\Pricequote
	*/
	public function delete($PriceQuoteCode,$DeleteType) {
		$this->apiAction = "delete";
		$this->methodParameters = array(
			"PriceQuoteCode" => $PriceQuoteCode,
			"DeleteType" => $DeleteType,
		);
		return $this;
	}

	/**
	 * @param string $PriceQuoteCode
	 * @return \Tcsehv\WeFact\Methods\Pricequote
	*/
	public function download($PriceQuoteCode) {
		$this->apiAction = "download";
		$this->methodParameters = array(
			"PriceQuoteCode" => $PriceQuoteCode,
		);
		return $this;
	}

	/**
	 * @param string $Identifier
	 * @param array $PriceQuoteLines
	 * @return \Tcsehv\WeFact\Methods\Pricequote
	*/
	public function edit($Identifier,$PriceQuoteLines) {
		$this->apiAction = "edit";
		$this->methodParameters = array(
			"Identifier" => $Identifier,
			"PriceQuoteLines" => $PriceQuoteLines,
		);
		return $this;
	}

	/**
	 * @param string $searchat
	 * @param string $searchfor
	 * @return \Tcsehv\WeFact\Methods\Pricequote
	*/
	public function getlist($searchat,$searchfor) {
		$this->apiAction = "list";
		$this->methodParameters = array(
			"searchat" => $searchat,
			"searchfor" => $searchfor,
		);
		return $this;
	}

	/**
	 * @param string $PriceQuoteCode
	 * @return \Tcsehv\WeFact\Methods\Pricequote
	*/
	public function sendbyemail($PriceQuoteCode) {
		$this->apiAction = "sendbyemail";
		$this->methodParameters = array(
			"PriceQuoteCode" => $PriceQuoteCode,
		);
		return $this;
	}

	/**
	 * @param string $PriceQuoteCode
	 * @return \Tcsehv\WeFact\Methods\Pricequote
	*/
	public function show($PriceQuoteCode) {
		$this->apiAction = "show";
		$this->methodParameters = array(
			"PriceQuoteCode" => $PriceQuoteCode,
		);
		return $this;
	}

}