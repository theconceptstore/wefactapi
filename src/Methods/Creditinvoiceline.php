<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
*/
class Creditinvoiceline extends Base implements ConnectionInterface {

	protected $apiController = "Creditinvoiceline";

	/**
	 * @param string $CreditInvoiceCode
	 * @param array $InvoiceLines
	 * @return \Tcsehv\WeFact\Methods\Creditinvoiceline
	*/
	public function add($CreditInvoiceCode,$InvoiceLines) {
		$this->apiAction = "add";
		$this->methodParameters = array(
			"CreditInvoiceCode" => $CreditInvoiceCode,
			"InvoiceLines" => $InvoiceLines,
		);
		return $this;
	}

	/**
	 * @param string $CreditInvoiceCode
	 * @param array $InvoiceLines
	 * @return \Tcsehv\WeFact\Methods\Creditinvoiceline
	*/
	public function delete($CreditInvoiceCode,$InvoiceLines) {
		$this->apiAction = "delete";
		$this->methodParameters = array(
			"CreditInvoiceCode" => $CreditInvoiceCode,
			"InvoiceLines" => $InvoiceLines,
		);
		return $this;
	}

}