<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
*/
class AttachmentCreditor extends Base implements ConnectionInterface {

	protected $apiController = "AttachmentCreditor";

	/**
	 * @param string $CreditorCode
	 * @param string $Type
	 * @param string $Filename
	 * @param string $Base64
	 * @return \Tcsehv\WeFact\Methods\AttachmentCreditor
	*/
	public function add($CreditorCode,$Type,$Filename,$Base64) {
		$this->apiAction = "add";
		$this->methodParameters = array(
			"CreditorCode" => $CreditorCode,
			"Type" => $Type,
			"Filename" => $Filename,
			"Base64" => $Base64,
		);
		return $this;
	}

	/**
	 * @param string $CreditorCode
	 * @param string $Type
	 * @param string $Filename
	 * @return \Tcsehv\WeFact\Methods\AttachmentCreditor
	*/
	public function delete($CreditorCode,$Type,$Filename) {
		$this->apiAction = "delete";
		$this->methodParameters = array(
			"CreditorCode" => $CreditorCode,
			"Type" => $Type,
			"Filename" => $Filename,
		);
		return $this;
	}

	/**
	 * @param string $CreditorCode
	 * @param string $Type
	 * @param string $Filename
	 * @return \Tcsehv\WeFact\Methods\AttachmentCreditor
	*/
	public function download($CreditorCode,$Type,$Filename) {
		$this->apiAction = "download";
		$this->methodParameters = array(
			"CreditorCode" => $CreditorCode,
			"Type" => $Type,
			"Filename" => $Filename,
		);
		return $this;
	}

}