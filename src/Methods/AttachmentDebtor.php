<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
*/
class AttachmentDebtor extends Base implements ConnectionInterface {

	protected $apiController = "AttachmentDebtor";

	/**
	 * @param string $DebtorCode
	 * @param string $Type
	 * @param string $Filename
	 * @param string $Base64
	 * @return \Tcsehv\WeFact\Methods\AttachmentDebtor
	*/
	public function add($DebtorCode,$Type,$Filename,$Base64) {
		$this->apiAction = "add";
		$this->methodParameters = array(
			"DebtorCode" => $DebtorCode,
			"Type" => $Type,
			"Filename" => $Filename,
			"Base64" => $Base64,
		);
		return $this;
	}

	/**
	 * @param string $DebtorCode
	 * @param string $Type
	 * @param string $Filename
	 * @return \Tcsehv\WeFact\Methods\AttachmentDebtor
	*/
	public function delete($DebtorCode,$Type,$Filename) {
		$this->apiAction = "delete";
		$this->methodParameters = array(
			"DebtorCode" => $DebtorCode,
			"Type" => $Type,
			"Filename" => $Filename,
		);
		return $this;
	}

	/**
	 * @param string $DebtorCode
	 * @param string $Type
	 * @param string $Filename
	 * @return \Tcsehv\WeFact\Methods\AttachmentDebtor
	*/
	public function download($DebtorCode,$Type,$Filename) {
		$this->apiAction = "download";
		$this->methodParameters = array(
			"DebtorCode" => $DebtorCode,
			"Type" => $Type,
			"Filename" => $Filename,
		);
		return $this;
	}

}