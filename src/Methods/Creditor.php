<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
*/
class Creditor extends Base implements ConnectionInterface {

	protected $apiController = "Creditor";

	/**
	 * @param string $CompanyName
	 * @param string $Initials
	 * @param string $SurName
	 * @param string $Sex
	 * @param string $Address
	 * @param string $ZipCode
	 * @param string $City
	 * @param string $Country
	 * @param string $EmailAddress
	 * @return \Tcsehv\WeFact\Methods\Creditor
	*/
	public function add($CompanyName,$Initials,$SurName,$Sex,$Address,$ZipCode,$City,$Country,$EmailAddress) {
		$this->apiAction = "add";
		$this->methodParameters = array(
			"CompanyName" => $CompanyName,
			"Initials" => $Initials,
			"SurName" => $SurName,
			"Sex" => $Sex,
			"Address" => $Address,
			"ZipCode" => $ZipCode,
			"City" => $City,
			"Country" => $Country,
			"EmailAddress" => $EmailAddress,
		);
		return $this;
	}

	/**
	 * @param string $Identifier
	 * @return \Tcsehv\WeFact\Methods\Creditor
	*/
	public function delete($Identifier) {
		$this->apiAction = "delete";
		$this->methodParameters = array(
			"Identifier" => $Identifier,
		);
		return $this;
	}

	/**
	 * @param string $Identifier
	 * @param string $Initials
	 * @param string $SurName
	 * @return \Tcsehv\WeFact\Methods\Creditor
	*/
	public function edit($Identifier,$Initials,$SurName) {
		$this->apiAction = "edit";
		$this->methodParameters = array(
			"Identifier" => $Identifier,
			"Initials" => $Initials,
			"SurName" => $SurName,
		);
		return $this;
	}

	/**
	 * @param string $searchfor
	 * @return \Tcsehv\WeFact\Methods\Creditor
	*/
	public function getlist($searchfor) {
		$this->apiAction = "list";
		$this->methodParameters = array(
			"searchfor" => $searchfor,
		);
		return $this;
	}

	/**
	 * @param string $CreditorCode
	 * @return \Tcsehv\WeFact\Methods\Creditor
	*/
	public function show($CreditorCode) {
		$this->apiAction = "show";
		$this->methodParameters = array(
			"CreditorCode" => $CreditorCode,
		);
		return $this;
	}

}