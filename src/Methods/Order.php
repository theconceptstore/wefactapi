<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;
use Tcsehv\WeFact\WeFactApi;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
 */
class Order extends Base implements ConnectionInterface
{

    /* Shipping methods */
    const SEND_BY_EMAIL = 0;
    const SEND_BY_POST = 1;
    const SEND_BY_POST_AND_EMAIL = 3;

    /* Invoice statuses */
    const STATUS_CONCEPT = 0;
    const STATUS_SENT = 2;
    const STATUS_PARTIALLY_PAID = 3;
    const STATUS_PAID = 4;
    const STATUS_CREDIT_INVOICE = 8;
    const STATUS_EXPIRED = 9;

    protected $apiController = "Order";

    /**
     * @param int $debtorId
     * @param int $productId
     * @param int $setupCostsProductId
     * @param null|string|int $domainNameId
     * @param int|null $tldProductId
     * @param null|string $websiteUrl
     * @param int|null $compensateProductId
     * @param int $invoiceMethod - Check Order for more options
     * @return Order
     * @throws Exception
     */
    public function add($debtorId, $productId, $setupCostsProductId, $domainNameId, $tldProductId = null, $websiteUrl = null, $compensateProductId = null, $invoiceMethod = Order::SEND_BY_EMAIL)
    {
        if (is_numeric($debtorId) && is_numeric($productId)) {
            $orderLines = [];

            // Get chosen product and create order line
            $chosenProduct = WeFactApi::product()->showProductById($productId)->execute();
            if(empty($chosenProduct->errors)) {
                $orderLines[] = ['ProductCode' => $chosenProduct->product->ProductCode];
            }

            // Get product for setup costs
            $setupCostsProduct = WeFactApi::product()->showProductById($setupCostsProductId)->execute();
            if(empty($setupCostsProduct->errors)) {
                $orderLines[] = ['ProductCode' => $setupCostsProduct->product->ProductCode];
            }

            // Get domain product
            $domainProduct = !empty($tldProductId) ? WeFactApi::product()->showProductById($tldProductId)->execute() : null;
            if(empty($domainProduct->errors)) {
                $orderLineDomain = ['ProductCode' => $domainProduct->product->ProductCode, 'Description' => '.' . $domainProduct->product->ProductTld . '-domeinnaam voor de volgende website: ' . $websiteUrl];

                if(is_numeric($domainNameId)) {
                    $domainNameId = intval($domainNameId);
                    $orderLineDomain['Reference'] = $domainNameId;
                }
                $orderLines[] = $orderLineDomain;
            }

            // Check if there is a compensating product available and create order line
            if (!empty($compensateProductId)) {
                $compensateDepositProduct = WeFactApi::product()->showProductById($compensateProductId)->execute();
                if (empty($compensateDepositProduct->errors)) {
                    $orderLines[] = ['ProductCode' => $compensateDepositProduct->product->ProductCode];
                }
            }

            $this->apiAction = "add";
            $this->methodParameters = [
                'Debtor' => $debtorId,
                'IgnoreDiscount' => 1,
                'InvoiceMethod' => $invoiceMethod,
                'Authorisation' => 'no',
                'Status' => Order::STATUS_CONCEPT,
                'OrderLines' => $orderLines
            ];
        }
        return $this;
    }

}