<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
*/
class AttachmentInvoice extends Base implements ConnectionInterface {

	protected $apiController = "AttachmentInvoice";

	/**
	 * @param string $InvoiceCode
	 * @param string $Type
	 * @param string $Filename
	 * @param string $Base64
	 * @return \Tcsehv\WeFact\Methods\AttachmentInvoice
	*/
	public function add($InvoiceCode,$Type,$Filename,$Base64) {
		$this->apiAction = "add";
		$this->methodParameters = array(
			"InvoiceCode" => $InvoiceCode,
			"Type" => $Type,
			"Filename" => $Filename,
			"Base64" => $Base64,
		);
		return $this;
	}

	/**
	 * @param string $InvoiceCode
	 * @param string $Type
	 * @param string $Filename
	 * @return \Tcsehv\WeFact\Methods\AttachmentInvoice
	*/
	public function delete($InvoiceCode,$Type,$Filename) {
		$this->apiAction = "delete";
		$this->methodParameters = array(
			"InvoiceCode" => $InvoiceCode,
			"Type" => $Type,
			"Filename" => $Filename,
		);
		return $this;
	}

	/**
	 * @param string $InvoiceCode
	 * @param string $Type
	 * @param string $Filename
	 * @return \Tcsehv\WeFact\Methods\AttachmentInvoice
	*/
	public function download($InvoiceCode,$Type,$Filename) {
		$this->apiAction = "download";
		$this->methodParameters = array(
			"InvoiceCode" => $InvoiceCode,
			"Type" => $Type,
			"Filename" => $Filename,
		);
		return $this;
	}

}