<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;
use Tcsehv\WeFact\WeFactApi;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
 */
class Invoice extends Base implements ConnectionInterface
{

    /* Shipping methods */
    const SEND_BY_EMAIL = 0;
    const SEND_BY_POST = 1;
    const SEND_BY_POST_AND_EMAIL = 3;

    /* Invoice statuses */
    const STATUS_CONCEPT = 0;
    const STATUS_SENT = 2;
    const STATUS_PARTIALLY_PAID = 3;
    const STATUS_PAID = 4;
    const STATUS_CREDIT_INVOICE = 8;
    const STATUS_EXPIRED = 9;

    protected $apiController = "Invoice";

    /**
     * Create invoice for deposit and sent to provided debtor
     *
     * @param int $debtorId
     * @param int $chosenProductId
     * @param int $depositProductId
     * @param int $shippingMethod - Check Invoice for more options
     * @param int $invoiceStatus - Check Invoice for more options
     * @return Invoice
     * @throws Exception
     */
    public function sendDeposit($debtorId, $chosenProductId, $depositProductId, $shippingMethod = Invoice::SEND_BY_EMAIL, $invoiceStatus = Invoice::STATUS_SENT)
    {
        if (is_numeric($debtorId) && is_numeric($chosenProductId) && is_numeric($depositProductId)) {
            $dateTime = new \DateTime();

            $product = WeFactApi::product()->showProductById($chosenProductId)->execute();
            $depositProduct = WeFactApi::product()->showProductById($depositProductId)->execute();

            if (!empty($product) && !empty($depositProduct)) {
                $this->apiAction = 'add';
                $this->methodParameters = [
                    'Debtor' => $debtorId,
                    'Date' => $dateTime->format('Y-m-d'),
                    'InvoiceMethod' => $shippingMethod,
                    'SentDate' => $dateTime->format('Y-m-d'),
                    'Authorisation' => 'no',
                    'Description' => 'Aanbetaling voor het volgende pakket: ' . $product->product->ProductName,
                    'Status' => $invoiceStatus,
                    'InvoiceLines' => [
                        [
                            'ProductCode' => $depositProduct->product->ProductCode
                        ]
                    ]
                ];
            }
        }
        return $this;
    }

    /**
     * Send provided invoice by email
     *
     * @param int|string $invoiceId
     * @return Invoice
     */
    public function sendByEmail($invoiceId) {
        $this->apiAction = 'sendbyemail';

        if(is_numeric($invoiceId)) {
            $this->methodParameters = ['Identifier' => $invoiceId];
        } else {
            $this->methodParameters = ['InvoiceCode' => $invoiceId];
        }
        return $this;
    }
}