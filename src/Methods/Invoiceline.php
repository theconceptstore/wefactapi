<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
*/
class Invoiceline extends Base implements ConnectionInterface {

	protected $apiController = "Invoiceline";

	/**
	 * @param string $InvoiceCode
	 * @param array $InvoiceLines
	 * @return \Tcsehv\WeFact\Methods\Invoiceline
	*/
	public function add($InvoiceCode,$InvoiceLines) {
		$this->apiAction = "add";
		$this->methodParameters = array(
			"InvoiceCode" => $InvoiceCode,
			"InvoiceLines" => $InvoiceLines,
		);
		return $this;
	}

	/**
	 * @param string $InvoiceCode
	 * @param array $InvoiceLines
	 * @return \Tcsehv\WeFact\Methods\Invoiceline
	*/
	public function delete($InvoiceCode,$InvoiceLines) {
		$this->apiAction = "delete";
		$this->methodParameters = array(
			"InvoiceCode" => $InvoiceCode,
			"InvoiceLines" => $InvoiceLines,
		);
		return $this;
	}

}