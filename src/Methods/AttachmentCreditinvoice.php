<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
*/
class AttachmentCreditinvoice extends Base implements ConnectionInterface {

	protected $apiController = "AttachmentCreditinvoice";

	/**
	 * @param string $CreditInvoiceCode
	 * @param string $Type
	 * @param string $Filename
	 * @param string $Base64
	 * @return \Tcsehv\WeFact\Methods\AttachmentCreditinvoice
	*/
	public function add($CreditInvoiceCode,$Type,$Filename,$Base64) {
		$this->apiAction = "add";
		$this->methodParameters = array(
			"CreditInvoiceCode" => $CreditInvoiceCode,
			"Type" => $Type,
			"Filename" => $Filename,
			"Base64" => $Base64,
		);
		return $this;
	}

	/**
	 * @param string $CreditInvoiceCode
	 * @param string $Type
	 * @return \Tcsehv\WeFact\Methods\AttachmentCreditinvoice
	*/
	public function delete($CreditInvoiceCode,$Type) {
		$this->apiAction = "delete";
		$this->methodParameters = array(
			"CreditInvoiceCode" => $CreditInvoiceCode,
			"Type" => $Type,
		);
		return $this;
	}

	/**
	 * @param string $CreditInvoiceCode
	 * @param string $Type
	 * @return \Tcsehv\WeFact\Methods\AttachmentCreditinvoice
	*/
	public function download($CreditInvoiceCode,$Type) {
		$this->apiAction = "download";
		$this->methodParameters = array(
			"CreditInvoiceCode" => $CreditInvoiceCode,
			"Type" => $Type,
		);
		return $this;
	}

}