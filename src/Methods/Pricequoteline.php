<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
*/
class Pricequoteline extends Base implements ConnectionInterface {

	protected $apiController = "Pricequoteline";

	/**
	 * @param string $PriceQuoteCode
	 * @param array $PriceQuoteLines
	 * @return \Tcsehv\WeFact\Methods\Pricequoteline
	*/
	public function add($PriceQuoteCode,$PriceQuoteLines) {
		$this->apiAction = "add";
		$this->methodParameters = array(
			"PriceQuoteCode" => $PriceQuoteCode,
			"PriceQuoteLines" => $PriceQuoteLines,
		);
		return $this;
	}

	/**
	 * @param string $PriceQuoteCode
	 * @param array $PriceQuoteLines
	 * @return \Tcsehv\WeFact\Methods\Pricequoteline
	*/
	public function delete($PriceQuoteCode,$PriceQuoteLines) {
		$this->apiAction = "delete";
		$this->methodParameters = array(
			"PriceQuoteCode" => $PriceQuoteCode,
			"PriceQuoteLines" => $PriceQuoteLines,
		);
		return $this;
	}

}