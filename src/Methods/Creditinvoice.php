<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
*/
class Creditinvoice extends Base implements ConnectionInterface {

	protected $apiController = "Creditinvoice";

	/**
	 * @param string $CreditorCode
	 * @param string $InvoiceCode
	 * @param string $Date
	 * @param array $InvoiceLines
	 * @return \Tcsehv\WeFact\Methods\Creditinvoice
	*/
	public function add($CreditorCode,$InvoiceCode,$Date,$InvoiceLines) {
		$this->apiAction = "add";
		$this->methodParameters = array(
			"CreditorCode" => $CreditorCode,
			"InvoiceCode" => $InvoiceCode,
			"Date" => $Date,
			"InvoiceLines" => $InvoiceLines,
		);
		return $this;
	}

	/**
	 * @param string $Identifier
	 * @return \Tcsehv\WeFact\Methods\Creditinvoice
	*/
	public function delete($Identifier) {
		$this->apiAction = "delete";
		$this->methodParameters = array(
			"Identifier" => $Identifier,
		);
		return $this;
	}

	/**
	 * @param string $CreditInvoiceCode
	 * @param string $Term
	 * @return \Tcsehv\WeFact\Methods\Creditinvoice
	*/
	public function edit($CreditInvoiceCode,$Term) {
		$this->apiAction = "edit";
		$this->methodParameters = array(
			"CreditInvoiceCode" => $CreditInvoiceCode,
			"Term" => $Term,
		);
		return $this;
	}

	/**
	 * @param string $searchat
	 * @param string $searchfor
	 * @return \Tcsehv\WeFact\Methods\Creditinvoice
	*/
	public function getlist($searchat,$searchfor) {
		$this->apiAction = "list";
		$this->methodParameters = array(
			"searchat" => $searchat,
			"searchfor" => $searchfor,
		);
		return $this;
	}

	/**
	 * @param string $CreditInvoiceCode
	 * @return \Tcsehv\WeFact\Methods\Creditinvoice
	*/
	public function markaspaid($CreditInvoiceCode) {
		$this->apiAction = "markaspaid";
		$this->methodParameters = array(
			"CreditInvoiceCode" => $CreditInvoiceCode,
		);
		return $this;
	}

	/**
	 * @param string $CreditInvoiceCode
	 * @param string $AmountPaid
	 * @return \Tcsehv\WeFact\Methods\Creditinvoice
	*/
	public function partpayment($CreditInvoiceCode,$AmountPaid) {
		$this->apiAction = "partpayment";
		$this->methodParameters = array(
			"CreditInvoiceCode" => $CreditInvoiceCode,
			"AmountPaid" => $AmountPaid,
		);
		return $this;
	}

	/**
	 * @param string $CreditInvoiceCode
	 * @return \Tcsehv\WeFact\Methods\Creditinvoice
	*/
	public function show($CreditInvoiceCode) {
		$this->apiAction = "show";
		$this->methodParameters = array(
			"CreditInvoiceCode" => $CreditInvoiceCode,
		);
		return $this;
	}

}