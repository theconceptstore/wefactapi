<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
 */
class Product extends Base implements ConnectionInterface {

    protected $apiController = "Product";

    /**
     * TODO: Add missing fields (https://www.wefact.nl/wefact-hosting/apiv2/producten/add)
     *
     * @param string $ProductName
     * @param string $ProductKeyPhrase
     * @param string $PriceExcl
     * @return \Tcsehv\WeFact\Methods\Product
     */
    public function add($ProductName,$ProductKeyPhrase,$PriceExcl) {
        $this->apiAction = "add";
        $this->methodParameters = array(
            "ProductName" => $ProductName,
            "ProductKeyPhrase" => $ProductKeyPhrase,
            "PriceExcl" => $PriceExcl,
        );
        return $this;
    }

    /**
     * @param string|int $id
     * @return \Tcsehv\WeFact\Methods\Product
     */
    public function delete($id) {
        if(is_numeric($id)) {
            $product = $this->showProductById($id);
        } else {
            $product = $this->showProductByProductCode($id);
        }

        if(!empty($product['product']['Identifier'])) {
            $this->apiAction = "delete";
            $this->methodParameters = ["Identifier" => $product['product']['Identifier']];
        }
        return $this;
    }

    /**
     * TODO: Add missing fields (https://www.wefact.nl/wefact-hosting/apiv2/producten/edit)
     *
     * @param string $Identifier
     * @param string $PriceExcl
     * @return \Tcsehv\WeFact\Methods\Product
     */
    public function edit($Identifier,$PriceExcl) {
        $this->apiAction = "edit";
        $this->methodParameters = array(
            "Identifier" => $Identifier,
            "PriceExcl" => $PriceExcl,
        );
        return $this;
    }

    /**
     * @return \Tcsehv\WeFact\Methods\Product
     */
    public function getlist() {
        $allowedParams = ['offset', 'limit', 'sort', 'order', 'searchat', 'searchfor', 'group'];
        $params = $this->checkParams($allowedParams);

        $this->apiAction = "list";
        $this->methodParameters = $params;

        return $this;
    }

    /**
     * @param int $id
     * @return \Tcsehv\WeFact\Methods\Product
     * @throws Exception
     */
    public function showProductById($id) {
        $this->apiAction = "show";
        $this->methodParameters = ['Identifier' => $id];

        return $this;
    }

    /**
     * @param string $productCode
     * @return \Tcsehv\WeFact\Methods\Product
     * @throws Exception
     */
    public function showProductByProductCode($productCode) {
        $this->apiAction = "show";
        $this->methodParameters = ['ProductCode' => $productCode];

        return $this;
    }
}