<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
*/
class Ticket extends Base implements ConnectionInterface {

	protected $apiController = "Ticket";

	/**
	 * @param string $Debtor
	 * @param string $Subject
	 * @param array $TicketMessages
	 * @return \Tcsehv\WeFact\Methods\Ticket
	*/
	public function add($Debtor,$Subject,$TicketMessages) {
		$this->apiAction = "add";
		$this->methodParameters = array(
			"Debtor" => $Debtor,
			"Subject" => $Subject,
			"TicketMessages" => $TicketMessages,
		);
		return $this;
	}

	/**
	 * @param string $TicketID
	 * @param array $TicketMessages
	 * @return \Tcsehv\WeFact\Methods\Ticket
	*/
	public function addmessage($TicketID,$TicketMessages) {
		$this->apiAction = "addmessage";
		$this->methodParameters = array(
			"TicketID" => $TicketID,
			"TicketMessages" => $TicketMessages,
		);
		return $this;
	}

	/**
	 * @param string $TicketID
	 * @param string $Owner
	 * @return \Tcsehv\WeFact\Methods\Ticket
	*/
	public function changeowner($TicketID,$Owner) {
		$this->apiAction = "changeowner";
		$this->methodParameters = array(
			"TicketID" => $TicketID,
			"Owner" => $Owner,
		);
		return $this;
	}

	/**
	 * @param string $TicketID
	 * @param string $Status
	 * @return \Tcsehv\WeFact\Methods\Ticket
	*/
	public function changestatus($TicketID,$Status) {
		$this->apiAction = "changestatus";
		$this->methodParameters = array(
			"TicketID" => $TicketID,
			"Status" => $Status,
		);
		return $this;
	}

	/**
	 * @param string $TicketID
	 * @return \Tcsehv\WeFact\Methods\Ticket
	*/
	public function delete($TicketID) {
		$this->apiAction = "delete";
		$this->methodParameters = array(
			"TicketID" => $TicketID,
		);
		return $this;
	}

	/**
	 * @param string $TicketID
	 * @param string $Subject
	 * @return \Tcsehv\WeFact\Methods\Ticket
	*/
	public function edit($TicketID,$Subject) {
		$this->apiAction = "edit";
		$this->methodParameters = array(
			"TicketID" => $TicketID,
			"Subject" => $Subject,
		);
		return $this;
	}

	/**
	 * @param string $searchfor
	 * @return \Tcsehv\WeFact\Methods\Ticket
	*/
	public function getlist($searchfor) {
		$this->apiAction = "list";
		$this->methodParameters = array(
			"searchfor" => $searchfor,
		);
		return $this;
	}

	/**
	 * @param string $TicketID
	 * @return \Tcsehv\WeFact\Methods\Ticket
	*/
	public function show($TicketID) {
		$this->apiAction = "show";
		$this->methodParameters = array(
			"TicketID" => $TicketID,
		);
		return $this;
	}

}