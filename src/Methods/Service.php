<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
*/
class Service extends Base implements ConnectionInterface {

	protected $apiController = "Service";

	/**
	 * @param string $DebtorCode
	 * @param array $Subscription
	 * @return \Tcsehv\WeFact\Methods\Service
	*/
	public function add($DebtorCode,$Subscription) {
		$this->apiAction = "add";
		$this->methodParameters = array(
			"DebtorCode" => $DebtorCode,
			"Subscription" => $Subscription,
		);
		return $this;
	}

	/**
	 * @param string $Identifier
	 * @param array $Subscription
	 * @return \Tcsehv\WeFact\Methods\Service
	*/
	public function edit($Identifier,$Subscription) {
		$this->apiAction = "edit";
		$this->methodParameters = array(
			"Identifier" => $Identifier,
			"Subscription" => $Subscription,
		);
		return $this;
	}

	/**
	 * @param string $searchfor
	 * @return \Tcsehv\WeFact\Methods\Service
	*/
	public function getlist($searchfor) {
		$this->apiAction = "list";
		$this->methodParameters = array(
			"searchfor" => $searchfor,
		);
		return $this;
	}

	/**
	 * @param string $Identifier
	 * @return \Tcsehv\WeFact\Methods\Service
	*/
	public function show($Identifier) {
		$this->apiAction = "show";
		$this->methodParameters = array(
			"Identifier" => $Identifier,
		);
		return $this;
	}

	/**
	 * @param string $Identifier
	 * @return \Tcsehv\WeFact\Methods\Service
	*/
	public function terminate($Identifier) {
		$this->apiAction = "terminate";
		$this->methodParameters = array(
			"Identifier" => $Identifier,
		);
		return $this;
	}

}