<?php
namespace Tcsehv\WeFact\Methods;

use Exception;
use Tcsehv\WeFact\Base;
use Tcsehv\WeFact\ConnectionInterface;
use Tcsehv\WeFact\WeFactApi;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 * @package WeFactApi
 */
class Domain extends Base implements ConnectionInterface
{

    protected $apiController = "Domain";

    const HANDLE_INFORMATION_HIDE = 'no';
    const HANDLE_INFORMATION_SHOW = 'yes';

    const STATUS_IN_ORDER = -1;
    const STATUS_WAITING_FOR_ACTION = 1;
    const STATUS_REQUEST = 3;
    const STATUS_ACTIVE = 4;
    const STATUS_EXPIRED = 5;
    const STATUS_PENDING = 6;
    const STATUS_ERROR_OCCURRED = 7;
    const STATUS_CANCELED = 8;
    const STATUS_REMOVED = 9;

    /**
     * @param int $debtorId
     * @param int $domainExtensionProductId
     * @param string $domain
     * @param int $status
     * @param null|string $comment
     * @return Domain
     * @throws Exception
     */
    public function add($debtorId, $domainExtensionProductId, $domain, $status = Domain::STATUS_WAITING_FOR_ACTION, $comment = null)
    {
        if(is_numeric($debtorId) && is_numeric($domainExtensionProductId) && !empty($domain)) {
            $explodedDomain = explode('.', $domain);

            $domainExtensionProduct = WeFactApi::product()->showProductById($domainExtensionProductId)->execute();

            $this->apiAction = "add";
            $this->methodParameters = [
                'Debtor' => $debtorId,
                'Domain' => $explodedDomain[0],
                'Tld' => $explodedDomain[1],
                'HasSubscription' => 'yes',
                'Status' => $status,
                'Comment' => $comment,
                'Subscription' => [
                    'ProductCode' => $domainExtensionProduct->product->ProductCode
                ]
            ];
        }
        return $this;
    }

    /**
     * @param string $Domain
     * @param string $Tld
     * @param string $DNS1
     * @param string $DNS2
     * @return \Tcsehv\WeFact\Methods\Domain
     */
    public function changenameserver($Domain, $Tld, $DNS1, $DNS2)
    {
        $this->apiAction = "changenameserver";
        $this->methodParameters = array(
            "Domain" => $Domain,
            "Tld" => $Tld,
            "DNS1" => $DNS1,
            "DNS2" => $DNS2,
        );
        return $this;
    }

    /**
     * @param string $Domain
     * @param string $Tld
     * @return \Tcsehv\WeFact\Methods\Domain
     */
    public function check($Domain, $Tld)
    {
        $this->apiAction = "check";
        $this->methodParameters = array(
            "Domain" => $Domain,
            "Tld" => $Tld,
        );
        return $this;
    }

    /**
     * @param string $Identifier
     * @return \Tcsehv\WeFact\Methods\Domain
     */
    public function delete($Identifier)
    {
        $this->apiAction = "delete";
        $this->methodParameters = array(
            "Identifier" => $Identifier,
        );
        return $this;
    }

    /**
     * @param string $Domain
     * @param string $Tld
     * @param string $Status
     * @return \Tcsehv\WeFact\Methods\Domain
     */
    public function edit($Domain, $Tld, $Status)
    {
        $this->apiAction = "edit";
        $this->methodParameters = array(
            "Domain" => $Domain,
            "Tld" => $Tld,
            "Status" => $Status,
        );
        return $this;
    }

    /**
     * @param string $Domain
     * @param string $Tld
     * @return \Tcsehv\WeFact\Methods\Domain
     */
    public function gettoken($Domain, $Tld)
    {
        $this->apiAction = "gettoken";
        $this->methodParameters = array(
            "Domain" => $Domain,
            "Tld" => $Tld,
        );
        return $this;
    }

    /**
     * @return $this
     */
    public function getlist()
    {
        $allowedParams = ['status', 'offset', 'limit', 'sort', 'order', 'searchat', 'searchfor'];
        $params = $this->checkParams($allowedParams);

        $this->apiAction = "list";
        $this->methodParameters = $params;

        return $this;
    }

    /**
     * @param string $Domain
     * @param string $Tld
     * @return \Tcsehv\WeFact\Methods\Domain
     */
    public function lock($Domain, $Tld)
    {
        $this->apiAction = "lock";
        $this->methodParameters = array(
            "Domain" => $Domain,
            "Tld" => $Tld,
        );
        return $this;
    }

    /**
     * @param string $Domain
     * @param string $Tld
     * @return \Tcsehv\WeFact\Methods\Domain
     */
    public function register($Domain, $Tld)
    {
        $this->apiAction = "register";
        $this->methodParameters = array(
            "Domain" => $Domain,
            "Tld" => $Tld,
        );
        return $this;
    }

    /**
     * @param string $Domain
     * @param string $Tld
     * @return \Tcsehv\WeFact\Methods\Domain
     */
    public function syncwhois($Domain, $Tld)
    {
        $this->apiAction = "syncwhois";
        $this->methodParameters = array(
            "Domain" => $Domain,
            "Tld" => $Tld,
        );
        return $this;
    }

    /**
     * @param int $id
     * @param \DateTime $dateTime
     * @return Domain
     */
    public function terminate($id, \DateTime $dateTime)
    {
        $formattedDate = $dateTime->format('Y-m-d');

        $this->apiAction = "terminate";
        $this->methodParameters = ['Identifier' => $id, 'Date' => $formattedDate];

        return $this;
    }

    /**
     * @param string|int $domain
     * @param string $tld
     * @param string $authKey
     * @return Domain
     */
    public function transfer($domain, $tld, $authKey)
    {
        if(is_numeric($domain)) {
            $this->methodParameters = ['Identifier' => $domain, 'Tld' => $tld, 'AuthKey' => $authKey];
        } else {
            $this->methodParameters = ['Domain' => $domain, 'Tld' => $tld, 'AuthKey' => $authKey];
        }

        $this->apiAction = "transfer";

        return $this;
    }

    /**
     * @param string $Domain
     * @param string $Tld
     * @return \Tcsehv\WeFact\Methods\Domain
     */
    public function unlock($Domain, $Tld)
    {
        $this->apiAction = "unlock";
        $this->methodParameters = array(
            "Domain" => $Domain,
            "Tld" => $Tld,
        );
        return $this;
    }

    /**
     * @param string $name
     * @param string $showHandleInfo
     * @return $this
     */
    public function showDomainByName($name, $showHandleInfo = 'no')
    {
        // Verify handle information
        $showHandleInfo = $this->determineHandleInfo($showHandleInfo);
        $urlData = explode('.', $name);

        $this->apiAction = "show";
        $this->methodParameters = ['Domain' => $urlData[0], 'Tld' => $urlData[1], 'ShowHandleInfo' => $showHandleInfo];

        return $this;
    }

    /**
     * @param int $id
     * @param string $showHandleInfo
     * @return $this
     */
    public function showDomainById($id, $showHandleInfo = 'no')
    {
        // Verify handle information
        $showHandleInfo = $this->determineHandleInfo($showHandleInfo);

        $this->apiAction = "show";
        $this->methodParameters = ['Identifier' => $id, 'ShowHandleInfo' => $showHandleInfo];

        return $this;
    }

    /**
     * @param string $showHandleInfo
     * @return string
     */
    protected function determineHandleInfo($showHandleInfo)
    {
        return ($showHandleInfo != self::HANDLE_INFORMATION_HIDE || $showHandleInfo != self::HANDLE_INFORMATION_SHOW) ? self::HANDLE_INFORMATION_HIDE : $showHandleInfo;
    }

}