<?php
namespace Tcsehv\WeFact;

use Exception;

/**
 * WeFact Hosting API V2 Helper
 *
 * @copyright 2015 The Concept Store
 * @author Peter Meijer <p.meijer@theconceptstore.nl>
 *
 * Example:
 * WeFactApi::domain()->check($params);
 *
 * @package WeFactApi
 * @method static \TcsEhv\WeFact\Methods\Attachment attachment()
 * @method static \TcsEhv\WeFact\Methods\AttachmentCreditinvoice attachmentCreditinvoice()
 * @method static \TcsEhv\WeFact\Methods\AttachmentCreditor attachmentCreditor()
 * @method static \TcsEhv\WeFact\Methods\AttachmentDebtor attachmentDebtor()
 * @method static \TcsEhv\WeFact\Methods\AttachmentInvoice attachmentInvoice()
 * @method static \TcsEhv\WeFact\Methods\AttachmentPricequote attachmentPricequote()
 * @method static \TcsEhv\WeFact\Methods\Creditinvoice creditinvoice()
 * @method static \TcsEhv\WeFact\Methods\Creditinvoiceline creditinvoiceline()
 * @method static \TcsEhv\WeFact\Methods\Creditor creditor()
 * @method static \TcsEhv\WeFact\Methods\Debtor debtor()
 * @method static \TcsEhv\WeFact\Methods\Domain domain()
 * @method static \TcsEhv\WeFact\Methods\Handle handle()
 * @method static \TcsEhv\WeFact\Methods\Hosting hosting()
 * @method static \TcsEhv\WeFact\Methods\Invoice invoice()
 * @method static \TcsEhv\WeFact\Methods\Invoiceline invoiceline()
 * @method static \TcsEhv\WeFact\Methods\Order order()
 * @method static \TcsEhv\WeFact\Methods\Orderline orderline()
 * @method static \TcsEhv\WeFact\Methods\Pricequote pricequote()
 * @method static \TcsEhv\WeFact\Methods\Pricequoteline pricequoteline()
 * @method static \TcsEhv\WeFact\Methods\Product product()
 * @method static \TcsEhv\WeFact\Methods\Service service()
 * @method static \TcsEhv\WeFact\Methods\Ticket ticket()
 */
class WeFactApi
{

    public static $acceptedMethods = array("attachment", "attachmentCreditinvoice", "attachmentCreditor", "attachmentDebtor", "attachmentInvoice", "attachmentPricequote", "creditinvoice", "creditinvoiceline", "creditor", "debtor", "domain", "handle", "hosting", "invoice", "invoiceline", "order", "orderline", "pricequote", "pricequoteline", "product", "service", "ticket");

    public static function __callStatic($function, $arguments)
    {
        $class = __NAMESPACE__ . '\\Methods\\' . ucfirst($function);
        if (in_array($function, static::$acceptedMethods)) {

            if(getenv('WE_FACT_API_URL') !== false && getenv('WE_FACT_API_KEY') !== false) {
                /** @var Base $object */
                $object = new $class();
                $object->setApiConfig(getenv('WE_FACT_API_URL'), getenv('WE_FACT_API_KEY'));

                return $object;
            } else {
                throw new \RuntimeException('Please be sure environment variables WE_FACT_API_URL and WE_FACT_API_KEY exists');
            }
        } else {
            throw new Exception('Requested class ' . $class . ' does not exists');
        }
    }
}